#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;
//template <typename pizza>
struct Node;

struct Krawedz;

//template <typename pizza>

//template <typename pizza>
struct Krawedz {
	int dane;
	Node *poczatek;
	Node *koniec;
	Krawedz();
	Krawedz(Node *jeden, Node *dwa);
	Krawedz(int cos, Node *jeden, Node *dwa);
};

//template <typename pizza>
Krawedz::Krawedz() {
	dane = 0;
	poczatek = nullptr;
	koniec = nullptr;
};

Krawedz::Krawedz(Node *jeden, Node *dwa) {
	dane = 0;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
Krawedz::Krawedz(int cos, Node *jeden, Node *dwa) {
	dane = cos;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
struct Heap {
	int rozmiar;
	int pojemnosc;
	Krawedz* *T;
	Heap(int cos);
	void Heapify(Krawedz* T[], int heapSize, int index);
	void zamien(int l, int p);
	void insert(Krawedz* edge);
	Krawedz* usun();
};

Heap::Heap(int cos) {
	T = new Krawedz*[cos];
	pojemnosc = cos;
	rozmiar = 0;
};

void Heap::insert(Krawedz* edge)
{
	if (rozmiar == 0)
	{
		T[0] = edge;
		rozmiar = 1;
	}
	else
	{
		T[rozmiar] = edge;
		int indeks = rozmiar;
		rozmiar = rozmiar + 1;
		while (edge->dane < T[(indeks - 1) / 2]->dane)
		{
			zamien(indeks, ((indeks-1)/2));
			indeks = (indeks - 1) / 2;
		}
	}
}

Krawedz* Heap::usun()
{
	Krawedz* obiad = T[0];
	zamien(0,rozmiar-1);
	rozmiar = rozmiar - 1;
	Heapify(T,rozmiar,0);
	return obiad;
}

void Heap::Heapify(Krawedz* T[], int heapSize, int index)
{
	int left = (index + 1) * 2 - 1;
	int right = (index + 1) * 2;
	int largest = left;

	if (left < heapSize && T[left]->dane < T[index]->dane)
		largest = left;
	else
		largest = index;

	if (right < heapSize && T[right]->dane < T[largest]->dane)
		largest = right;

	if (largest != index)
	{
		Krawedz* temp = T[index];
		T[index] = T[largest];
		T[largest] = temp;

		Heapify(T, heapSize, largest);
	}
}

void Heap::zamien(int l, int p)
{
	Krawedz* temp = T[p];
	T[p] = T[l];
	T[l] = temp;
}
/*
template <typename pizza>
void Heap<pizza>::Heap_Sort(pizza T[], int count)
{
	int heapSize = count;

	for (int p = (heapSize) / 2 - 1; p >= 0; p--)
	{
		Heapify(T, heapSize, p);
	}

	for (int i = count - 1; i >= 0; i--)
	{
		zamien(0, i);

		//heapSize--;
		Heapify(T, i, 0);
	}
}*/

struct Node {
	int dane;
	int indeks;
	vector<Krawedz*> lista_krawedzi;
	Node();
	Node(int cos);
};

//template <typename pizza>
Node::Node() {
	dane = 0;
	//tu_jestem = new Node;
};

//template <typename pizza>
Node::Node(int cos) {
	dane = cos;
	//tu_jestem = COS;
};

//template <typename pizza>
struct Graf {
	vector<Node*> lista_wezlow;
	vector<Krawedz*> lista_krawedzi;
	Node **endVertices(int cos);
	Node *opposite(int cos, int meaning_of_life);
	bool Sasiedzi(int Kargul, int Pawlak);
	void replace(int v, int x);
	void replace_kraw(int v, int x);
	void insert_Node(int cos);
	void insert_Kraw(int cos, Node* pol, Node* lop);
	void usun_krawedz(Krawedz *cos);
	void usun(int cos);
	void pokaz_swoje_krawedzie(int v);
	void pokaz_wszystkie_krawedzie();
	void pokaz_wszystkie_wezly();
	void generuj(int rozmiar, int gestosc);
};

Node** Graf::endVertices(int cos)
{
	int i = 0;
	Node** tab = new Node*[2];
	tab[0] = new Node();
	tab[1] = new Node();
	//cout << "AWBNDIUAW" << endl;
	//cout << lista_krawedzi[i]->dane << endl;
	while (i<lista_krawedzi.size() && cos != lista_krawedzi[i]->dane)
	{
		i++;
	}
	//cout << lista_krawedzi.size() << endl;
	if (i < lista_krawedzi.size())
	{
		tab[0] = lista_krawedzi[i]->poczatek;
		tab[1] = lista_krawedzi[i]->koniec;
		return tab;
	}
	else
	{
	cout << i << endl;
		cout << "Brak szukanej krawedzi." << endl;
		tab[0]->dane = 0;
		tab[1]->dane = 0;
		return tab;
	}
}

Node* Graf::opposite(int wezel, int cos)
{
	vector<Node*> elemencik = lista_wezlow;
	//Node *zakoncz = new Node;
	//zakoncz = *lista_wezlow->end();
	int i = 0;
	//cout << lista_wezlow.size() << endl;
	while (i < lista_wezlow.size() && elemencik[i]->dane != wezel)
	{
		i++;
		//cout << "POMOC" << endl;
	}
	vector<Krawedz*> elemencior = elemencik[i]->lista_krawedzi;
	//cout << elemencior->dane << endl;
	int j = 0;
	while (elemencior[j]->dane != cos)
		j++;
	if (wezel == elemencior[j]->poczatek->dane)
		return elemencior[j]->koniec;
	else if (wezel == elemencior[j]->koniec->dane)
		return elemencior[j]->poczatek;
	else
	{
		cout << "Cos poszlo nie tak." << endl;
		return elemencik[i];
	}
}

bool Graf::Sasiedzi(int Kargul, int Pawlak)
{
	Krawedz* element = lista_krawedzi[0];
	while (element != nullptr)
	{
		if (Kargul == element->poczatek->dane)
		{
			if (Pawlak == element->koniec->dane)
			{
				return true;
			}
		}
		else if (Kargul == element->koniec->dane)
		{
			if (Pawlak == element->poczatek->dane)
			{
				return true;
			}
		}
		else
			element++;
	}
	return false;
}

void Graf::replace(int v, int x)
{
	int i = 0;
	while (v != lista_wezlow[i]->dane)
		i++;
	lista_wezlow[i]->dane = x;
}

void Graf::replace_kraw(int v, int x)
{
	int i = 0;
	while (v != lista_krawedzi[i]->dane)
		i++;
	lista_krawedzi[i]->dane = x;
}

void Graf::insert_Node(int cos)
{
	int sukces = 0;
	//cout << "Utworzono wezel o wartosci " << cos << endl;
	for (int i = 0; i < lista_wezlow.size(); i++) {
		if (cos == lista_wezlow[i]->dane)
			sukces = 1;
	}
	if (sukces == 0)
	{
		Node *cos1 = new Node(cos);
		cos1->indeks = lista_wezlow.size();
		lista_wezlow.push_back(cos1);
	}
}

void Graf::insert_Kraw(int cos, Node* pol, Node* lop)
{
	int i = 0;
	Krawedz *krawedz = new Krawedz();
	krawedz->dane = cos;
	int indeks1 = 0;
	krawedz->poczatek = pol;
	krawedz->koniec = lop;
	lop->lista_krawedzi.push_back(krawedz);
	pol->lista_krawedzi.push_back(krawedz);
	lista_krawedzi.push_back(krawedz);
	//cout << "Uwtworzone krawedz " << cos << " o krawedziach " << pol << " i " << lop << endl;
}

void Graf::usun_krawedz(Krawedz *cos)
{
	int i = 0;
	cout << "Usunieto krawedz " << cos->dane << endl;
	while (i<cos->poczatek->lista_krawedzi.size() && cos != cos->poczatek->lista_krawedzi[i])
		i++;
	if (i < cos->poczatek->lista_krawedzi.size())
	{
		cos->poczatek->lista_krawedzi.erase(cos->poczatek->lista_krawedzi.begin() + i);
		i = 0;
		while (cos != cos->koniec->lista_krawedzi[i])
			i++;
		cos->koniec->lista_krawedzi.erase(cos->koniec->lista_krawedzi.begin() + i);
		i = 0;
		while (cos != lista_krawedzi[i])
			i++;
		lista_krawedzi.erase(lista_krawedzi.begin() + i);
		delete cos;
	}
	else
		cout << "Brak podanej krawedzi." << endl;
}

void Graf::usun(int cos)
{
	int j = 0;
	while (j < lista_wezlow.size() && cos != lista_wezlow[j]->dane)
		j++;
	if (j < lista_wezlow.size())
	{
		Node *jak = lista_wezlow[j];
		int i = 0;
		while (i < jak->lista_krawedzi.size())
		{
			usun_krawedz(jak->lista_krawedzi[i]);
			i++;
		}
		lista_wezlow.erase(lista_wezlow.begin() + i);
		delete jak;
	}
	else
		cout << "Brak poszukiwanego wezla." << endl;
}

void Graf::pokaz_swoje_krawedzie(int v)
{
	int i = 0;
	vector<Krawedz*> kop;
	cout << "Oto wszystkie krawedzie wezla " <<v<< endl;
	while (i < lista_wezlow.size() && v != lista_wezlow[i]->dane)
		i++;
	if (i < lista_wezlow.size())
	{
		kop = lista_wezlow[i]->lista_krawedzi;
		for (int j = 0; j < kop.size(); j++)
		{
			cout << kop[j]->dane << endl;
		}
	}
	else
		cout << "Brak szukanego wezla." << endl;
}

void Graf::pokaz_wszystkie_krawedzie()
{
	vector<Krawedz*> kop;
	kop = lista_krawedzi;
	cout << "Oto wszystkie krawedzie grafu." << endl;
	if (kop.size()>0)
	{
		for (int i = 0; i < kop.size(); i++) {
			cout << "Wezel: " << kop[i]->koniec->dane << " i " << kop[i]->poczatek->dane;
			cout << " Ich Krawedz: " << kop[i]->dane << endl;
		}
	}
	else
	{
		cout << "Brak krawedzi." << endl;
	}
}

void Graf::pokaz_wszystkie_wezly()
{
	vector<Node*> kop;
	kop = lista_wezlow;
	cout << "Oto wszystkie wezly grafu." << endl;
	if (kop.size()>0)
	{
		for (int i = 0; i < kop.size(); i++)
			cout << kop[i]->dane << endl;
	}
	else
	{
		cout << "Brak wezlow." << endl;
	}
}

void Graf::generuj(int rozmiar, int gestosc)
{
	//int key = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		insert_Node(i);
	}

	bool **macierz = new bool*[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		macierz[i] = new bool[rozmiar];
		for (int j = 0; j < rozmiar; j++)
		{
			macierz[i][j] = false;
		}
	}
	int jeden = rand() % rozmiar;
	int dwa = rand() % rozmiar;
	float pomoc = gestosc;
	int liczba = ( pomoc/ 100)*rozmiar*(rozmiar - 1) / 2;
	for (int i = 0; i < liczba; i++)
	{
		jeden = rand() % rozmiar;
		dwa = rand() % rozmiar;
		while (macierz[dwa][jeden] == true) {
			jeden = rand() % rozmiar;
			dwa = rand() % rozmiar;
		}
		if (macierz[dwa][jeden] == false) {
			insert_Kraw(rand() % 101, lista_wezlow[jeden], lista_wezlow[dwa]);
			macierz[jeden][dwa] = true;
			macierz[dwa][jeden] = true;
		}
	}
}

struct DSNode
{
	Node* up;
	int	rank;
};

class DSStruct
{
private:
	DSNode * Z;
public:
	DSStruct(int n);
	~DSStruct();
	void MakeSet(Node* v);
	Node* FindSet(Node* v);
	void UnionSets(Krawedz* e);
};

DSStruct::DSStruct(int n)
{
	Z = new DSNode[n];             
};

DSStruct::~DSStruct()
{
	delete[] Z;
};

void DSStruct::MakeSet(Node* v)//bylo int
{
	Z[v->indeks].up = v;
	Z[v->indeks].rank = 0;
}

Node* DSStruct::FindSet(Node* v)// bylo int
{
	if (Z[v->indeks].up->indeks != v->indeks) 
		Z[v->indeks].up = FindSet(Z[v->indeks].up);
	return Z[v->indeks].up;
}

void DSStruct::UnionSets(Krawedz* e)
{
	Node* ru = new Node;
	Node* rv = new Node;

	ru = FindSet(e->poczatek);             // Wyznaczamy korzeń drzewa z węzłem u
	rv = FindSet(e->koniec);             // Wyznaczamy korzeń drzewa z węzłem v
	if (ru != rv)                    // Korzenie muszą być różne
	{
		if (Z[ru->indeks].rank > Z[rv->indeks].rank)   // Porównujemy rangi drzew
			Z[rv->indeks].up = ru;              // ru większe, dołączamy rv
		else
		{
			Z[ru->indeks].up = rv;            // równe lub rv większe, dołączamy ru
			if (Z[ru->indeks].rank == Z[rv->indeks].rank) Z[rv->indeks].rank++;
		}
	}
}

//template<typename Krawedz*>
Graf* Kruskal(Graf *pierwotniak)
{
	Graf *pomoc = new Graf;
	Graf *wyjscie = new Graf;
	pomoc = pierwotniak;
	pomoc->lista_krawedzi = pierwotniak->lista_krawedzi;
	pomoc->lista_wezlow = pierwotniak->lista_wezlow;
	Krawedz *najmniej;
	int size = pierwotniak->lista_krawedzi.size();
	DSStruct Z(size);

	Heap *kopiec = new Heap(size);

	for (int i = 0; i < pomoc->lista_wezlow.size(); i++)
	{
		Z.MakeSet(pomoc->lista_wezlow[i]);
	}

	for (int i = 0; i < size; i++)
	{
		kopiec->insert(pomoc->lista_krawedzi[i]);
	}

	//cout << kopiec->T[0]->dane << endl;
	/*for (int i = 0; i < size; i++)
	{
		najmniej = kopiec->usun();
		cout << najmniej->dane << endl;
	}*/

	for (int i = 1; i < pomoc->lista_wezlow.size(); i++)
	{
		do
		{
			najmniej = kopiec->usun();
			//cout << najmniej->dane << endl;
		} while (Z.FindSet(najmniej->poczatek) == Z.FindSet(najmniej->koniec));
		//		cout << "pomoc" << endl;
		//cout << najmniej->koniec->dane << endl;
		wyjscie->insert_Node(najmniej->poczatek->dane);
		//cout << "POMOCY" << endl;
		wyjscie->insert_Node(najmniej->koniec->dane);
		wyjscie->insert_Kraw(najmniej->dane, najmniej->poczatek, najmniej->koniec);
		Z.UnionSets(najmniej);
	}
	return wyjscie;
}

//Graf *Prim(Graf *pierwotniak)

Graf* Prim(Graf* pierwotniak)
{
	Graf *pomoc = new Graf;
	Graf *wyjscie = new Graf;
	int sukces = 0;
	int licznik = 1;
	pomoc = pierwotniak;
	pomoc->lista_krawedzi = pierwotniak->lista_krawedzi;
	pomoc->lista_wezlow = pierwotniak->lista_wezlow;
	Krawedz *najmniej;
	//int size = pierwotniak->lista_krawedzi.size();
	//DSStruct Z(size);
	Heap *kopiec = new Heap(pomoc->lista_krawedzi.size());
	bool *sprawdzenie = new bool[pomoc->lista_wezlow.size()];
	for (int i = 0; i < pomoc->lista_wezlow.size(); i++)
	{
		sprawdzenie[i] = false;
	}
	sprawdzenie[0] = true;
	wyjscie->insert_Node(0);
	for (int i = 0; i < pomoc->lista_wezlow[0]->lista_krawedzi.size(); i++)
	{
		kopiec->insert(pomoc->lista_wezlow[0]->lista_krawedzi[i]);
	}

	while (licznik < pomoc->lista_wezlow.size())
	{

		najmniej = kopiec->usun();
		sukces = 0;

		if (sprawdzenie[najmniej->poczatek->dane] == true)
			sukces = sukces + 1;

		if (sprawdzenie[najmniej->koniec->dane] == true)
			sukces = sukces + 1;

		if (sukces < 2) {
			if (sprawdzenie[najmniej->poczatek->dane] == false) {
				wyjscie->insert_Node(najmniej->poczatek->dane);
				sprawdzenie[najmniej->poczatek->dane] = true;
				for (int j = 0; j < pomoc->lista_wezlow[najmniej->poczatek->dane]->lista_krawedzi.size(); j++)
				{
					if (sprawdzenie[pomoc->lista_wezlow[najmniej->poczatek->dane]->lista_krawedzi[j]->poczatek->dane] == false || sprawdzenie[pomoc->lista_wezlow[najmniej->poczatek->dane]->lista_krawedzi[j]->koniec->dane] == false)
					kopiec->insert(pomoc->lista_wezlow[najmniej->poczatek->dane]->lista_krawedzi[j]);
				}
				licznik++;
			}
			if (sprawdzenie[najmniej->koniec->dane] == false) {
				wyjscie->insert_Node(najmniej->koniec->dane);
				sprawdzenie[najmniej->koniec->dane] = true;
				for (int j = 0; j < pomoc->lista_wezlow[najmniej->koniec->dane]->lista_krawedzi.size(); j++)
				{
					if (sprawdzenie[pomoc->lista_wezlow[najmniej->koniec->dane]->lista_krawedzi[j]->poczatek->dane] == false || sprawdzenie[pomoc->lista_wezlow[najmniej->koniec->dane]->lista_krawedzi[j]->koniec->dane] == false)
					kopiec->insert(pomoc->lista_wezlow[najmniej->koniec->dane]->lista_krawedzi[j]);
				}
				licznik++;
			}
			wyjscie->insert_Kraw(najmniej->dane, najmniej->poczatek, najmniej->koniec);
		}
	}
	return wyjscie;
}

int main()
{
	Graf *Grafik = new Graf;
	Graf *pomoc = new Graf;
	clock_t t1, t2, TC = 0;
	/*Node *cos1 = new Node(12);
	Node *cos2 = new Node(2);
	Krawedz *kraw = new Krawedz(5, cos1, cos2);
	cos1->lista_krawedzi.push_back(kraw);
	cos2->lista_krawedzi.push_back(kraw);
	//cos1->lista_krawedzi->push_back(kraw);
	Grafik->lista_wezlow.push_back(cos2);
	Grafik->lista_wezlow.push_back(cos1);
	//kraw->dane = 5;
	//kraw->koniec = cos2;
	//kraw->poczatek = cos1;
	Grafik->lista_krawedzi.push_back(kraw);
	//cout << "COS" << endl;
	int kop = Grafik->opposite(2, 5)->dane;
	cout << kop << endl;
	cout << "Krawedzie sasiadujace do 5: " << endl;*/
	/*cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;
	cout << "Zastapiono element 2 przez 7." << endl;
	Grafik->replace(2, 7);
	cout << "Zastapiono krawedz 5 przez 9." << endl;
	Grafik->replace_kraw(5, 9);
	cout << "Krawedzie sasiadujace do 9: " << endl;
	tab = Grafik->endVertices(9);
	cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;*/
	//Grafik->insert_Kraw(6, 7, 8);
	//cout << "Krawedzie sasiadujace do 6: " << endl;
	//Node **tab = Grafik->endVertices(6);
	//cout << tab[0]->dane << endl;
	//cout << tab[1]->dane << endl;
	//cout<< Grafik->lista_krawedzi[1]->dane << endl;
	//Grafik->usun(7);
	//cout << Grafik->lista_krawedzi.size() << endl;
	//cout << "Usunieto krawedz 1." << endl;
	//Grafik->usun_krawedz(Grafik->lista_krawedzi[1]);
	//Grafik->insert_Kraw(18, 7, 20);
	//Grafik->insert_Kraw(99, 20, 11);
	//Grafik->insert_Kraw(1, 11, 8);
	//Grafik->insert_Kraw(77, 20, 8);
	cout << "cos" << endl;
	Grafik->generuj(100, 25);
	//Grafik->pokaz_wszystkie_wezly();
	//Grafik->pokaz_wszystie_krawedzie();
	//Grafik->pokaz_wszystkie_wezly();
	//Grafik->pokaz_wszystkie_krawedzie();
	//Grafik->pokaz_swoje_krawedzie(8);
	//Grafik->pokaz_wszystkie_krawedzie();

	pomoc->lista_krawedzi = Grafik->lista_krawedzi;
	pomoc->lista_wezlow = Grafik->lista_wezlow;
	//Node **tab1 = pomoc->endVertices(6);

	cout << "Krawedzie KRUSKAL" << endl;

	t1 = clock();
	Graf * cos = Kruskal(Grafik);
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	cout << "Oto liczba krawedzi: " << cos->lista_krawedzi.size() << endl;
	cout << "Oto liczba wezlow: " << cos->lista_wezlow.size() << endl;
	//cos->pokaz_wszystkie_krawedzie();
	//cos->pokaz_wszystkie_wezly();
	cout << "Krawedzie PRIM" << endl;

	t1 = clock();
	cos = Prim(Grafik);
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	//cos->pokaz_wszystkie_krawedzie();
	cout << "Oto liczba krawedzi: " << cos->lista_krawedzi.size() << endl;
	cout << "Oto liczba wezlow: " << cos->lista_wezlow.size() << endl;
	//cos->pokaz_wszystkie_krawedzie();
	//cout << "Krawedzie PRIM" << endl;
	//Prim(Grafik)->pokaz_wszystkie_krawedzie();
	return 0;
}
