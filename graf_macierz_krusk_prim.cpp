#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;
//template <typename pizza>
struct Node;

//template <typename pizza>
struct Krawedz {
	int dane;
	Node *poczatek;
	Node *koniec;
	Krawedz();
	Krawedz(Node *jeden, Node *dwa);
	Krawedz(int cos, Node *jeden, Node *dwa);
};

//template <typename pizza>
Krawedz::Krawedz() {
	dane = 0;
	poczatek = nullptr;
	koniec = nullptr;
};

Krawedz::Krawedz(Node *jeden, Node *dwa) {
	dane = 0;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
Krawedz::Krawedz(int cos, Node *jeden, Node *dwa) {
	dane = cos;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
struct Node {
	int dane;
	int indeks;
	int touch;
	Node();
	Node(int cos);
};

//template <typename pizza>
Node::Node() {
	dane = 0;
	touch = 0;
	//tu_jestem = new Node;
};

//template <typename pizza>
Node::Node(int cos) {
	dane = cos;
	touch = 0;
	//tu_jestem = COS;
};

//template <typename pizza>
struct Graf {
	vector<Node*> lista_wezlow;
	Krawedz* **matrix;
	int rozmiar;
	int ilosc;
	int indeks_wezla(int cos);
	Node **endVertices(Krawedz* cos);
	Node *opposite(int cos, int meaning_of_life);
	bool Sasiedzi(int Kargul, int Pawlak);
	void replace(int v, int x);
	void powieksz();
	void insert_Node(int cos);
	void insert_Kraw(int cos, Node *pol, Node *lop);
	void usun_krawedz(int cos);
	void usun(int cos);
	void pokaz_swoje_krawedzie(int v);
	void pokaz_wszystkie_krawedzie();
	void pokaz_wszystkie_wezly();
	void generuj(int rozmiar, int gestosc);
	Graf();
};

Graf::Graf()
{
	rozmiar = 1000;
	matrix = new Krawedz* *[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		matrix[i] = new Krawedz*[rozmiar];
		for (int j = 0; j < rozmiar; j++)
		{
			matrix[i][j] = nullptr;
		}
	}
	ilosc = 0;
};

Node** Graf::endVertices(Krawedz* cos)
{
	Node **tab = new Node*[2];
	if (cos != nullptr)
	{
		cout << "Krawedz o wadze " << cos->dane << " laczy wierzcholki " << cos->poczatek->dane << " i " << cos->koniec->dane << "." << endl;
		tab[0] = cos->poczatek;
		tab[1] = cos->koniec;
	}
	else
	{
		cout << "Niestety szukana krawedz nie istnieje." << endl;
		tab[0]->dane = 0;
		tab[1]->dane = 0;
	}
	return tab;
}

Node* Graf::opposite(int wezel, int cos)
{
	Node *pomoc = new Node;
	if (indeks_wezla(wezel) != -1)
	{
		int i = indeks_wezla(wezel);
		for (int j = 0; j < lista_wezlow.size(); j++)
		{
			if (matrix[i][j] != nullptr)
			{
				if (matrix[i][j]->dane == cos)
				{
					if (matrix[i][j]->poczatek->dane == wezel)
					{
						pomoc = matrix[i][j]->koniec;
						return pomoc;
					}
					else
					{
						pomoc = matrix[i][j]->poczatek;
						return pomoc;
					}
				}
			}
		}
		cout << endl;
	}
	else
	{
		cout << "Brak szukanego wezla." << endl;
	}
	return pomoc;
}

bool Graf::Sasiedzi(int Kargul, int Pawlak)
{
	if (matrix[indeks_wezla(Kargul)][indeks_wezla(Pawlak)] != nullptr)
	{
		cout << "Sa to sasiedzi." << endl;
		return true;
	}
	else if (matrix[indeks_wezla(Kargul)][indeks_wezla(Pawlak)] != nullptr)
	{
		cout << "Sa to sasiedzi." << endl;
		return true;
	}
	else
	{
		cout << "Nie sa sasiadami." << endl;
		return false;
	}
}

void Graf::replace(int v, int x)
{
	if (indeks_wezla(v) == -1)
	{
		cout << "Brak szukanego wezla." << endl;
	}
	else
	{
		lista_wezlow[indeks_wezla(v)]->dane = x;
		cout << "Zastapiono " << v << " przez " << x << "." << endl;
	}
}

/*
void Graf::replace_kraw(int v, int x)
{
int i = 0;
while (v != lista_krawedzi[i]->dane)
i++;
lista_krawedzi[i]->dane = x;
}
*/

int Graf::indeks_wezla(int cos)
{
	for (int i = 0; i < lista_wezlow.size(); i++)
	{
		if (lista_wezlow[i]->dane == cos)
			return i;
	}
	return -1;
}

void Graf::powieksz()
{
	rozmiar = rozmiar * 2;
	Krawedz* **Temp = new Krawedz* *[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		Temp[i] = new Krawedz*[rozmiar];
		for (int j = 0; j < rozmiar; j++)
			Temp[i][j] = nullptr;
	}
	for (int i = 0; i < rozmiar / 2; i++) {
		for (int j = 0; j < rozmiar / 2; j++)
			Temp[i][j] = matrix[i][j];
	}
	matrix = Temp;
}

void Graf::insert_Node(int cos)
{
	if (indeks_wezla(cos) == -1)
	{
		Node *wezel = new Node(cos);
		lista_wezlow.push_back(wezel);
		if (ilosc + 1 == rozmiar)
		{
			powieksz();
		}
		wezel->indeks = ilosc;
		ilosc++;
		//cout << "Dodano wierzcholek o wartosci " << cos << "." << endl;
	}
	//else
		//cout << "W grafie jest juz wierzcholek o podanej wartosci!" << endl;
}

void Graf::insert_Kraw(int cos, Node* pol, Node* lop)
{/*
	if (indeks_wezla(pol) == -1)
	{
		insert_Node(pol);
	//cout << "pomoc1" << endl;
		lista_wezlow[pol]->touch = 1;
	}
	if (indeks_wezla(lop) == -1)
	{
		insert_Node(lop);
		lista_wezlow[lop]->touch = 1;
	}*/
	Krawedz *krawedz = new Krawedz(cos, pol, lop);
	matrix[pol->indeks][lop->indeks] = krawedz;
	matrix[lop->indeks][pol->indeks] = krawedz;
//	cout << "Do wierzcholkow " << pol << " i " << lop << " dodano krawedz o wartosci " << cos << endl;
}

void Graf::usun_krawedz(int cos)
{
	int usunieto = 0;
	if (lista_wezlow.size() == 0) {
		cout << "Graf jest pusty!" << endl;
	}
	else
	{
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			for (int j = 0; j < lista_wezlow.size(); j++)
			{
				if (matrix[i][j] != nullptr) {
					if (matrix[i][j]->dane == cos)
					{
						matrix[i][j] = nullptr;
						usunieto++;
					}
				}
			}
		}
		if (usunieto == 0)
			cout << "Podana krawedz nie istnieje!\n";
		else
			cout << "Krawedz o wartosci " << cos << " zostala usunieta\n";
	}
}

void Graf::usun(int cos)
{
	int indeks = indeks_wezla(cos);
	if (indeks == -1)
		cout << "Niestety, nie ma akiego wierzcholka." << endl;
	else
	{
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			if (matrix[indeks][i] != nullptr)
				usun_krawedz(matrix[indeks][i]->dane);
		}
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			for (int j = 0; j < lista_wezlow.size(); j++)
			{
				if (i > indeks && j > indeks)
				{
					matrix[i - 1][j - 1] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
				else if (i > indeks) {
					matrix[i - 1][j] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
				else if (j > indeks) {
					matrix[i][j - 1] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
			}
		}
		lista_wezlow.erase(lista_wezlow.begin() + indeks);
		for (int i = indeks; i < lista_wezlow.size(); i++)
			lista_wezlow[i]->indeks--;
		ilosc--;
		cout << "Wierzcholek o wartosci " << cos << " zostal usuniety." << endl;
	}
}

void Graf::pokaz_swoje_krawedzie(int v)
{
	if (indeks_wezla(v) != -1)
	{
		int i = indeks_wezla(v);
		cout << "Krawedzie wierzcholka " << lista_wezlow[i]->dane << ": " << endl;
		for (int j = 0; j < lista_wezlow.size(); j++)
		{
			if (matrix[i][j] != nullptr)
				cout << matrix[i][j]->dane << endl;
		}
		cout << endl;
	}
	else
	{
		cout << "Brak wybranego wezla." << endl;
	}
}

void Graf::pokaz_wszystkie_krawedzie()
{
	if (lista_wezlow.size() != 0)
	{
		cout << endl << "Wszystkie krawedzie w grafie:" << endl;
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			//cout << "Krawedzie wierzcholka " << lista_wezlow[i]->dane << ": ";
			for (int j = 0; j < lista_wezlow.size(); j++)
			{
				if (matrix[i][j] != nullptr)
					cout << matrix[i][j]->dane << " ";
				if (matrix[i][j] == nullptr)
					cout << 0 << " ";
			}
			cout << endl;
		}
	}
	else
	{
		cout << "Brak wezlow w grafie." << endl;
	}
}

void Graf::pokaz_wszystkie_wezly()
{
	if (lista_wezlow.size() != 0)
	{
		cout << endl << "Wszystkie wierzcholki w grafie:" << endl;;
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			cout << lista_wezlow[i]->dane << endl;
		}
		cout << endl;
	}
	else
	{
		cout << "Graf jest pusty." << endl;
	}
}

void Graf::generuj(int rozmiar, int gestosc)
{/*
	int key = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		insert_Node(key);
		key = key + 1;
	}

	float pomoc = gestosc;
	int liczba = (pomoc / 100)*rozmiar*(rozmiar - 1) / 2;
	int jeden = 0;
	int dwa = 0;
	for (int i = 0; i < liczba; i++)
	{
		jeden = rand() % rozmiar;
		dwa = rand() % rozmiar;
		while(dwa==jeden)
			dwa=rand()%rozmiar;
		insert_Kraw(rand() % 101, lista_wezlow[jeden]->dane, lista_wezlow[dwa]->dane);
	}
	
	for (int i = 0; i < lista_wezlow.size();i++)
	{
		if (lista_wezlow[i]->touch == 0) {
			while (dwa == lista_wezlow[i]->dane)
				dwa = rand() % rozmiar;
			insert_Kraw(rand() % 101, lista_wezlow[i]->dane, lista_wezlow[dwa]->dane);
		}
	}*/
	int key = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		insert_Node(key);
		key = key + 1;
	}

	float pomoc = gestosc;
	int liczba = (pomoc / 100)*rozmiar*(rozmiar - 1) / 2;
	for (int i = 0; i < liczba; i++)
	{
		insert_Kraw(rand() % 101, lista_wezlow[rand() % rozmiar], lista_wezlow[rand() % rozmiar]);
	}
}

struct Heap {
	int rozmiar;
	int pojemnosc;
	Krawedz* *T;
	Heap(int cos);
	void Heapify(Krawedz* T[], int heapSize, int index);
	void zamien(int l, int p);
	void insert(Krawedz* edge);
	Krawedz* usun();
	//void Heap_Sort(Krawedz* T[], int count);
};

Heap::Heap(int cos) {
	T = new Krawedz*[cos];
	pojemnosc = cos;
	rozmiar = 0;
};

void Heap::insert(Krawedz* edge)
{
	if (rozmiar == 0)
	{
		T[0] = edge;
		rozmiar = 1;
	}
	else
	{
		T[rozmiar] = edge;
		int indeks = rozmiar;
		rozmiar = rozmiar + 1;
		while (edge->dane < T[(indeks - 1) / 2]->dane)
		{
			zamien(indeks, ((indeks - 1) / 2));
			indeks = (indeks - 1) / 2;
		}
	}
}

Krawedz* Heap::usun()
{
	Krawedz* obiad = T[0];
	zamien(0, rozmiar - 1);
	rozmiar = rozmiar - 1;
	Heapify(T, rozmiar, 0);
	return obiad;
}

void Heap::Heapify(Krawedz* T[], int heapSize, int index)
{
	int left = (index + 1) * 2 - 1;
	int right = (index + 1) * 2;
	int largest = left;

	if (left < heapSize && T[left]->dane < T[index]->dane)
		largest = left;
	else
		largest = index;

	if (right < heapSize && T[right]->dane < T[largest]->dane)
		largest = right;

	if (largest != index)
	{
		Krawedz* temp = T[index];
		T[index] = T[largest];
		T[largest] = temp;

		Heapify(T, heapSize, largest);
	}
}

void Heap::zamien(int l, int p)
{
	Krawedz* temp = T[p];
	T[p] = T[l];
	T[l] = temp;
}

struct DSNode
{
	Node* up;
	int	rank;
};

class DSStruct
{
private:
	DSNode * Z;
public:
	DSStruct(int n);
	~DSStruct();
	void MakeSet(Node* v);
	Node* FindSet(Node* v);
	void UnionSets(Krawedz* e);
};

DSStruct::DSStruct(int n)
{
	Z = new DSNode[n];
};

DSStruct::~DSStruct()
{
	delete[] Z;
};

void DSStruct::MakeSet(Node* v)//bylo int
{
	Z[v->indeks].up = v;
	Z[v->indeks].rank = 0;
}

Node* DSStruct::FindSet(Node* v)// bylo int
{
	if (Z[v->indeks].up->indeks != v->indeks)
		Z[v->indeks].up = FindSet(Z[v->indeks].up);
	return Z[v->indeks].up;
}

void DSStruct::UnionSets(Krawedz* e)
{
	Node* ru = new Node;
	Node* rv = new Node;

	ru = FindSet(e->poczatek);             // Wyznaczamy korzeń drzewa z węzłem u
	rv = FindSet(e->koniec);             // Wyznaczamy korzeń drzewa z węzłem v
	if (ru != rv)                    // Korzenie muszą być różne
	{
		if (Z[ru->indeks].rank > Z[rv->indeks].rank)   // Porównujemy rangi drzew
			Z[rv->indeks].up = ru;              // ru większe, dołączamy rv
		else
		{
			Z[ru->indeks].up = rv;            // równe lub rv większe, dołączamy ru
			if (Z[ru->indeks].rank == Z[rv->indeks].rank) Z[rv->indeks].rank++;
		}
	}
}

Graf* Kruskal(Graf *pierwotniak)
{
	Graf *pomoc = new Graf;
	Graf *wyjscie = new Graf;
	pomoc = pierwotniak;
	pomoc->matrix = pierwotniak->matrix;
	pomoc->lista_wezlow = pierwotniak->lista_wezlow;
	Krawedz *najmniej;
	//int size = pierwotniak->lista_krawedzi.size();
	DSStruct Z(pomoc->lista_wezlow.size());

	Heap *kopiec = new Heap(pomoc->lista_wezlow.size()*pomoc->lista_wezlow.size());

	for (int i = 0; i < pomoc->lista_wezlow.size(); i++)
	{
		Z.MakeSet(pomoc->lista_wezlow[i]);
	}

	for (int i = 0; i <pomoc->lista_wezlow.size(); i++)
	{
		for (int j = 0; j < pomoc->lista_wezlow.size(); j++) {
			if (pomoc->matrix[i][j] != nullptr)
				kopiec->insert(pomoc->matrix[i][j]);
		}
	}

	for (int i = 1; i < pomoc->lista_wezlow.size(); i++)
	{
		do
		{
			najmniej = kopiec->usun();
		} while (Z.FindSet(najmniej->poczatek) == Z.FindSet(najmniej->koniec));
			//cout << najmniej->koniec->dane << endl;
		wyjscie->insert_Node(najmniej->poczatek->dane);
		wyjscie->insert_Node(najmniej->koniec->dane);
		wyjscie->insert_Kraw(najmniej->dane, najmniej->poczatek, najmniej->koniec);
	//cout << "POMOCYWAWA" << endl;
		Z.UnionSets(najmniej);
	}
	return wyjscie;
}

Graf* Prim(Graf* pierwotniak)
{
	Graf *pomoc = new Graf;
	Graf *wyjscie = new Graf;
	int sukces = 0;
	int licznik = 1;
	pomoc = pierwotniak;
	pomoc->matrix= pierwotniak->matrix;
	pomoc->lista_wezlow = pierwotniak->lista_wezlow;
	Krawedz *najmniej;
	//int size = pierwotniak->lista_krawedzi.size();
	//DSStruct Z(size);
	Heap *kopiec = new Heap(pomoc->lista_wezlow.size()*(pomoc->lista_wezlow.size()-1)/2);
	bool *sprawdzenie = new bool[pomoc->lista_wezlow.size()];
	for (int i = 0; i < pomoc->lista_wezlow.size(); i++)
	{
		sprawdzenie[i] = false;
	}
	sprawdzenie[0] = true;
	wyjscie->insert_Node(0);
	for (int i = 0; i < pomoc->lista_wezlow.size(); i++)
	{
		if(pomoc->matrix[0][i]!=nullptr)
		kopiec->insert(pomoc->matrix[0][i]);
	}

	while (licznik < pomoc->lista_wezlow.size())
	{

		najmniej = kopiec->usun();
		sukces = 0;

		if (sprawdzenie[najmniej->poczatek->dane] == true)
			sukces = sukces + 1;

		if (sprawdzenie[najmniej->koniec->dane] == true)
			sukces = sukces + 1;

		if (sukces < 2) {
			if (sprawdzenie[najmniej->poczatek->dane] == false) {
				wyjscie->insert_Node(najmniej->poczatek->dane);
				sprawdzenie[najmniej->poczatek->dane] = true;
				for (int j = 0; j < pomoc->lista_wezlow.size(); j++)
				{
					if (pomoc->matrix[najmniej->poczatek->dane][j] != nullptr) {
						if (sprawdzenie[pomoc->matrix[najmniej->poczatek->dane][j]->poczatek->dane] == false || sprawdzenie[pomoc->matrix[najmniej->poczatek->dane][j]->koniec->dane] == false)
							kopiec->insert(pomoc->matrix[najmniej->poczatek->dane][j]);
					}
				}
				licznik++;
			}
			if (sprawdzenie[najmniej->koniec->dane] == false) {
				wyjscie->insert_Node(najmniej->koniec->dane);
				sprawdzenie[najmniej->koniec->dane] = true;
				for (int j = 0; j < pomoc->lista_wezlow.size(); j++)
				{
					if (pomoc->matrix[najmniej->koniec->dane][j] != nullptr) {
						//cout << j << endl;
						//if (sprawdzenie[pomoc->matrix[najmniej->koniec->dane][j]->poczatek->dane] == false)
						//cout << "pOIAMDW" << endl;
						//cout << "kal" << endl;
						if (sprawdzenie[pomoc->matrix[najmniej->koniec->dane][j]->poczatek->dane] == false || sprawdzenie[pomoc->matrix[najmniej->koniec->dane][j]->koniec->dane] == false)
							kopiec->insert(pomoc->matrix[najmniej->koniec->dane][j]);
					}
				}
				licznik++;
			}
			wyjscie->insert_Kraw(najmniej->dane, najmniej->poczatek, najmniej->koniec);
		}
	}
	return wyjscie;
}

int main()
{
	Graf *Grafik = new Graf;
	clock_t t1, t2, TC = 0;
	/*
	Grafik->insert_Kraw(6, 7, 8);
	Grafik->insert_Kraw(17, 7, 5);
	Grafik->insert_Kraw(18, 21, 22);
	Grafik->insert_Kraw(2, 3, 4);
	Grafik->insert_Kraw(5, 1, 2);
	Grafik->insert_Kraw(50, 7, 71);
	Grafik->insert_Kraw(900, 7, 212);
	Grafik->insert_Kraw(180, 20, 7);
	
	cout << "poksoa" << endl;
	Node *cos = Grafik->opposite(7, 6);
	Grafik->pokaz_swoje_krawedzie(7);
	Grafik->pokaz_wszystkie_wezly();
	Grafik->usun(7);
	Grafik->pokaz_wszystkie_wezly();*/
	Grafik->generuj(10,100);
	//Grafik->pokaz_wszystkie_wezly();
	Grafik->pokaz_wszystkie_krawedzie();
	cout << "KRUSKAL" << endl;
	t1 = clock();
	Graf *pomoc = Kruskal(Grafik);
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	pomoc->pokaz_wszystkie_krawedzie();
	//Graf * kolos;
	cout << "PRIM" << endl;
	t1 = clock();
	pomoc = Prim(Grafik);
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	pomoc->pokaz_wszystkie_krawedzie();
	//pomoc->pokaz_wszystkie_wezly();
	//pomoc->pokaz_wszystkie_krawedzie();
	//cout << "das" << endl;
	//cout << Grafik->matrix[0][1]->dane << endl;
	return 0;
}